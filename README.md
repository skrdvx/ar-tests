## Setup
1. To start Docker from project root folder:
```
docker-compose up -d
```

2. To install dependencies from theme folder:

```
composer install
npm install

```

## Time log
+ clean theme = 1h
+ setup project = 20m
+ add post type = 20m
+ create blocks = 50m
+ update styles = 30m
+ update fields = 30m
+ update templates = 90m
+ update content = 30m
+ update styles = 2h
+ update scripts = 30m

Total: 8h